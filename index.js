require('dotenv').config();
const http = require('http');
const chalk = require('chalk');
const app = require('./src/app');
const logger = require('./src/lib/logger');
const setupRabbitMQ = require('./src/utils/distributedTransaction');

const port = process.env.PORT || 3000;
const server = http.createServer(app);

server.listen(port, () =>
    logger.info(
        chalk.bgGreen(`Server running on port: ${chalk.bgGreen(port)} `)
    ),
    // setupRabbitMQ(app)
);