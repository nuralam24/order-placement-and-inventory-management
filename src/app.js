const express = require('express');
const { createConnection } = require('typeorm'); // Import createConnection from TypeORM

const routes = require('./routes');
const middlewares = require('./middlewares');
const errorHandle = require('./middlewares/errorHandle')
const db = require('./config/db');

const app = express();
app.enable('trust proxy');
middlewares(app);
createConnection(db)
routes(app);
errorHandle(app);

module.exports = app;
