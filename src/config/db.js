const dotenv = require('dotenv');
dotenv.config();

const db = {
  type: 'postgres',
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT || 5432,
  username: process.env.DB_USERNAME || 'username',
  password: process.env.DB_PASSWORD || 'password',
  database: process.env.DB_DATABASE || 'ecommerce',
  synchronize: true,
  logging: false,
};

module.exports = db;