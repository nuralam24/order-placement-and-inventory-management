const { getRepository } = require('typeorm');
const Order = require('../models/user');
const OrderItem = require('../models/orderItem');
const Inventory = require('../models/inventory');
const User = require('../models/user');

const createOrder = async (req, res) => {
  const connection = await getRepository(Order).manager.connection;
  const queryRunner = connection.createQueryRunner();
  await queryRunner.startTransaction();

  try {
    const { userId, items } = req.body;
    const user = await getRepository(User).findOne(userId);

    if (!user) {
      throw new Error('User not found');
    }

    const order = new Order();
    order.user = user;
    order.status = 'pending';
    await queryRunner.manager.save(order);

    for (const item of items) {
      const product = await queryRunner.manager.findOne(Inventory, item.productId);

      if (product.stock < item.quantity) {
        throw new Error('Insufficient stock');
      }

      product.stock -= item.quantity;
      await queryRunner.manager.save(product);

      const orderItem = new OrderItem();
      orderItem.order = order;
      orderItem.product = product;
      orderItem.quantity = item.quantity;
      orderItem.price = item.price;
      await queryRunner.manager.save(orderItem);
    }

    await queryRunner.commitTransaction();
    res.status(200).send({ orderId: order.id });
  } catch (error) {
    await queryRunner.rollbackTransaction();
    res.status(400).send({ error: error.message });
  } finally {
    await queryRunner.release();
  }
};

module.exports = {
  createOrder,
};
