const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const session = require('express-session');
const morgan = require('morgan');
const log = require('../lib/logger');
const serverRequest = require('./requestAttempt');

const sessionConfig = {
    secret: process.env.TOKEN_SECRET,
    resave: false,
    saveUninitialized: true,
}

module.exports = (app) => {
    app.use(cors());
    app.use(serverRequest);
    app.set('trust proxy', 1);
    app.use(session(sessionConfig));
    app.use('/public', [express.static('public')]);
    app.use(helmet({ crossOriginResourcePolicy: false }));
    app.use(express.json({ limit: '50mb', extended: true }));
    app.use(express.urlencoded({ limit: '50mb', extended: true }));
    app.use(morgan('dev', { stream: { write: (m) => log.http(m.trim()) } }));
};
