const rateLimit = require('express-rate-limit');
const redis = require('redis');
const { RateLimiterRedis } = require('rate-limiter-flexible');

// Create Redis client
const redisClient = redis.createClient({
  host: process.env.REDIS_HOST || 'localhost',
  port: process.env.REDIS_PORT || 6379,
});

// Configure rate limiter
const rateLimiter = new RateLimiterRedis({
  storeClient: redisClient,
  keyPrefix: 'middleware',
  points: 10, // 10 requests
  duration: 60, // per 60 seconds
});

// Express rate limiting middleware
const serverRequest = (req, res, next) => {
  rateLimiter.consume(req.ip)
    .then(() => {
      next();
    })
    .catch(() => {
      res.status(429).send('Too many request attempts. Please try again after 1 minute!');
    });
};

module.exports = serverRequest;
