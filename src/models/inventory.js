const { EntitySchema } = require('typeorm');

const Inventory = new EntitySchema({
  name: 'Inventory',
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    name: {
      type: 'varchar',
    },
    price: {
      type: 'decimal',
      precision: 10,
      scale: 2,
    },
    stock: {
      type: 'int',
    },
  },
});

module.exports = Inventory;
