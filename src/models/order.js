const { EntitySchema } = require('typeorm');
const User = require('./user');
const OrderItem = require('./orderItem');

const Order = new EntitySchema({
  name: 'Order',
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    status: {
      type: 'varchar',
    },
    createdAt: {
      type: 'timestamp',
      createDate: true,
    },
  },
  relations: {
    user: {
      target: 'User',
      type: 'many-to-one',
      inverseSide: 'orders',
    },
    items: {
      target: 'OrderItem',
      type: 'one-to-many',
      inverseSide: 'order',
    },
  },
});

module.exports = Order;
