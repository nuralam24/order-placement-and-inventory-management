const { EntitySchema } = require('typeorm');
const Order = require('./order');
const Inventory = require('./inventory');

const orderItem = new EntitySchema({
  name: 'OrderItem',
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    quantity: {
      type: 'int',
    },
  },
  relations: {
    order: {
      target: 'Order',
      type: 'many-to-one',
      inverseSide: 'items',
    },
    inventory: {
      target: 'Inventory',
      type: 'many-to-one',
    },
  },
});

module.exports = orderItem;
