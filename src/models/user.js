const { EntitySchema } = require('typeorm');

const User = new EntitySchema({
  name: 'User',
  columns: {
    id: {
      primary: true,
      type: 'int',
      generated: true,
    },
    name: {
      type: 'varchar',
    },
    email: {
      type: 'varchar',
    },
  },
  relations: {
    orders: {
      target: 'Order',
      type: 'one-to-many',
      inverseSide: 'user',
    },
  },
});

module.exports = User;
