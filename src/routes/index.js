const routes = [
    { path: '/order', controller: require('./order') },
]

module.exports = (app) => {
    for (const route of routes) {
        app.use(`/api/v1${route.path}`, route.controller)
    }
};
