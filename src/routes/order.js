const router = require('express').Router()
const { celebrate } = require('celebrate')
const orderController = require('../controllers/orderController')

const verifyToken = require('../middlewares/verifyToken');
const permission = require('../middlewares/permission');


router.route('/order')
    .post(
        orderController.createOrder
    )

module.exports = router;