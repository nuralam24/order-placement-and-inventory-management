const request = require('supertest');
const app = require('../../index'); // Your Express app

describe('Order API', () => {
  it('should create an order', async () => {
    const response = await request(app)
      .post('/order')
      .send({
        userId: 1,
        items: [{ productId: 1, quantity: 2, price: 10.0 }]
      });
    expect(response.statusCode).toBe(200);
    expect(response.body.orderId).toBeDefined();
  });
});
