const amqp = require('amqplib/callback_api');
require('dotenv').config();

const RABBITMQ_URL = process.env.RABBITMQ_URL || 'amqp://localhost';

function createRabbitMQConnection() {
    return new Promise((resolve, reject) => {
        amqp.connect(RABBITMQ_URL, (err, conn) => {
            if (err) {
                return reject(err);
            }
            resolve(conn);
        });
    });
}

function createRabbitMQChannel(connection) {
    return new Promise((resolve, reject) => {
        connection.createChannel((err, channel) => {
            if (err) {
                return reject(err);
            }
            resolve(channel);
        });
    });
}

async function setupRabbitMQ(app) {
    try {
        const connection = await createRabbitMQConnection();
        const channel = await createRabbitMQChannel(connection);
        const exchange = 'order_exchange';

        channel.assertExchange(exchange, 'topic', { durable: true });

        app.post('/order', async (req, res) => {
            const order = { userId: req.body.userId, items: req.body.items };

            channel.publish(exchange, 'order.created', Buffer.from(JSON.stringify(order)));
            res.status(200).send({ message: 'Order placed' });
        });

        // Payment processing
        channel.assertQueue('order_payment', { durable: true }, (err, q) => {
            if (err) throw err;
            channel.bindQueue(q.queue, exchange, 'order.created');

            channel.consume(q.queue, (msg) => {
                const order = JSON.parse(msg.content.toString());
                // Simulate payment processing
                console.log('Processing payment for order', order);
                channel.publish(exchange, 'payment.processed', Buffer.from(JSON.stringify(order)));
            }, { noAck: true });
        });

        // Shipping processing
        channel.assertQueue('order_shipping', { durable: true }, (err, q) => {
            if (err) throw err;
            channel.bindQueue(q.queue, exchange, 'payment.processed');

            channel.consume(q.queue, (msg) => {
                const order = JSON.parse(msg.content.toString());
                // Simulate shipping processing
                console.log('Processing shipping for order', order);
                channel.publish(exchange, 'shipping.completed', Buffer.from(JSON.stringify(order)));
            }, { noAck: true });
        });

    } catch (error) {
        console.error('Failed to set up RabbitMQ', error);
    }
}

module.exports = {
    setupRabbitMQ
};
