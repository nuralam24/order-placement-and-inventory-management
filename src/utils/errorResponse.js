const internalServerError = (res, logMsg, error) => {
    console.log(logMsg);
    console.log('Error = ', error);
    return res.status(500).json({
        data: null,
        success: false,
        message: "Internal Server Error Occurred!"
    });
};

const noDataFoundSuccess = (res) => {
    return res.status(400).json({
        data: [],
        success: true,
        message: "No data found!"
    });
};

const noDataFoundSuccessWithPagination = (res, resultsPerPage) => {
    return res.status(200).json({
        data: {
            resultsPerPage,
            totalDataLength: 0,
            data: []
        },
        success: true,
        message: "No data found!"
    });
};

const noDataFoundFailed = (res) => {
    return res.status(404).json({
        data: null,
        success: false,
        message: "No data found!"
    });
};

const alreadyExists = (res, msg) => {
    return res.status(409).json({
        data: null,
        success: false,
        message: `${msg} already exists!`
    });
};

const invalidRequest = (res, msg) => {
    return res.status(400).json({
        data: null,
        success: false,
        message: `Request invalid. ${msg}`
    });
};

const createSuccess = (res, data, msg) => {
    return res.status(201).json({
        data: data || '',
        success: true,
        message: `${msg}`
    });
};

const updateSuccess = (res, data, msg) => {
    return res.status(202).json({
        data: data || '',
        success: true,
        message: `${msg}`
    });
};

const dataViewSuccess = (res, data) => {
    return res.status(200).json({
        data: data || '',
        success: true,
        message: 'View successfully!'
    });
};

const dataViewSuccessWithPagination = (res, resultsPerPage, totalDataLength, data) => {
    return res.status(200).json({
        data: {
            resultsPerPage,
            totalDataLength,
            data
        },
        success: true,
        message: "View successfully!"
    });
};

const deleteSuccess = (res) => {
    return res.status(202).json({
        success: true,
        message: 'Deleted successfully!'
    });
};

const incorrectInputData = (res, msg) => {
    return res.status(401).json({
        data: null,
        success: false,
        message: `Request invalid. ${msg}`
    });
};

const loggedInSuccess = (res, data) => {
    return res.status(200).json({
        data,
        success: true,
        message: 'Logged in Successfully!',
    });
};

module.exports = { 
    internalServerError,
    noDataFoundSuccess,
    noDataFoundSuccessWithPagination,
    noDataFoundFailed,
    alreadyExists,
    createSuccess,
    updateSuccess,
    dataViewSuccess,
    dataViewSuccessWithPagination,
    invalidRequest,
    deleteSuccess,
    incorrectInputData,
    loggedInSuccess
};